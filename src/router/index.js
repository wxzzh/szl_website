import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AboutView from '../views/AboutView.vue'
import Details from '../views/Details.vue'
const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView,
      children: [{
        path: '/about/:id',
        name: 'about',
        component: AboutView,
        meta: {
          level: 2
        }
      },
      {
        path: '/list/:id',
        name: 'list',
        component: () => import("../views/List.vue"),
        meta: {
          level: 2
        }
      },
      {
        path: '/productList',
        name: 'productlist',
        component: () => import("../views/ProductList.vue"),
        meta: {
          level: 2
        }
      },
      {
        path: '/productList/details/:productId',
        name: 'productdetail',
        component: () => import("../views/Product.vue"),
        meta: {
          level: 3,
          title: '产品详情'
        }
      },
      {
        path: '/list/details/:columnCode',
        name: 'detail',
        component: Details,
        meta: {
          level: 3
        }
      }]
    }
  ]
})

export default router
