// 0: 富文本显示 1: 列表
const getChildName = (item) => {
    return item.columnName + '详情'
}
export function getRouteMenu(menu) {
    let menuList = [{ columnName: "首页", path: "/", columnId: '0' }]
    if (menu && menu.length > 0) {
        let navList = menu.map(item => {
            item.path = item.columnType == 0 ? 'about' : 'list'
            item.path === 'list' ? item.childName = getChildName(item) : null
            return { ...item, }
        })
        menuList = menuList.concat(navList)
    }
    return menuList
}